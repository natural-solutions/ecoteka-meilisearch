#!/bin/sh

#############
# osm names #
#############

## 0. create folder
mkdir /builder/administrative_type

## 1. decompress files
gzip -d administrative_type.csv.gz 

## 3. create id column
lines=$(wc -l < "administrative_type.csv")
touch tempfile
for i in `seq 1 $lines`; 
do if [ $i == 1 ]; 
   then echo "id"; 
   else echo $i; 
   fi; 
done > "ids"

paste -d "\t" "ids" "administrative_type.csv" > "administrative_type_id.csv" 
rm "ids"


## 2. split csv and transform to json
tail -n +2 administrative_type_id.csv | split -l 10000 - /builder/administrative_type/split_
for file in /builder/administrative_type/split_*
do
    head -n 1 /builder/administrative_type_id.csv > tmp_file
    cat "$file" >> tmp_file
    mv -f tmp_file "$file"
    csvtojson --delimiter="\t" "$file" > "$file".json
    rm $file
done

## 3. compress folder
tar czfv administrative_type.json.tgz /builder/administrative_type

################################
# global_tree_search_trees_1_5 #
################################
gzip -d global_tree_search_trees_1_5.csv.gz 
csvtojson --headers='["id","family","genus","species","canonical_name","taxauthor"]' global_tree_search_trees_1_5.csv > global_tree_search_trees_1_5.json