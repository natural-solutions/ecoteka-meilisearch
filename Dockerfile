FROM node:alpine as builder
WORKDIR /builder
RUN npm i -g csvtojson
COPY data/administrative_type.csv.gz .
COPY data/global_tree_search_trees_1_5.csv.gz .
COPY scripts/transform_files.sh .
RUN ./transform_files.sh

FROM getmeili/meilisearch as snapshot
WORKDIR /snapshot
RUN apk add curl 
COPY --from=builder /builder/administrative_type.json.tgz .
COPY --from=builder /builder/global_tree_search_trees_1_5.json .
COPY scripts/import.sh .
RUN MEILI_HTTP_PAYLOAD_SIZE_LIMIT=10000000000 /meilisearch & \
    ./import.sh

FROM getmeili/meilisearch
COPY --from=snapshot /snapshot/data.ms /data.ms