#!/bin/sh

tar xzfv administrative_type.json.tgz

curl \
  -X POST 'http://127.0.0.1:7700/indexes' \
  --data '{
    "uid": "taxa",
    "primaryKey": "id"
  }'

curl -X POST 'http://127.0.0.1:7700/indexes/taxa/documents' --data @"global_tree_search_trees_1_5.json"

curl \
  -X POST 'http://127.0.0.1:7700/indexes' \
  --data '{
    "uid": "osmname",
    "primaryKey": "id"
  }'

curl \
  -X POST 'http://127.0.0.1:7700/indexes/osmname/settings/searchable-attributes' \
  --data '[
      "name",
      "alternative_names",
      "country",
      "state"
  ]'

curl \
  -X POST 'http://127.0.0.1:7700/indexes/osmname/settings/ranking-rules' \
  --data '[
      "typo",
      "desc(importance)",
      "asc(place_rank)",
      "words",
      "proximity",
      "attribute",
      "wordsPosition",
      "exactness"
  ]'


tar xzfv /snapshot/administrative_type.json.tgz 

for file in builder/administrative_type/*.json
do
  updateId=$(curl -X POST 'http://127.0.0.1:7700/indexes/osmname/documents' --data @"$file")
  echo $updateId
done

